package ua.vydai.gdxdots;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "gdxDots";
        cfg.useGL20 = false;
        cfg.resizable = false;
        cfg.width = 800;
        cfg.height = 480;
        
        new LwjglApplication(new GdxDots(), cfg);
    }
}

package ua.vydai.gdxdots;

import ua.vydai.gdxdots.screen.GameScreen;
import ua.vydai.gdxdots.util.GameHelper;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;

public class GdxDots extends Game {
    // constant useful for logging
    private static final String LOG = GameHelper.getSimpleName(GdxDots.class);

    private Screen gameScreen = null;

    // a libgdx helper class that logs the current FPS each second
    private FPSLogger fpsLogger;

    public Screen getGameScreen() {
        if (gameScreen == null) {
            gameScreen = new GameScreen(this);
        }
        return gameScreen;
    }

    @Override
    public void create() {
        Gdx.app.log(LOG, "Creating game");
        setScreen(getGameScreen());
        
        fpsLogger = new FPSLogger();
    }

   @Override
    public void resize(int width, int height) {
       super.resize(width, height);
       Gdx.app.log(LOG, "Resizing game to: " + width + " x " + height);
    }

    @Override
    public void render() {
        super.render();
        
        // output the current FPS
        fpsLogger.log();
    }

    @Override
    public void pause() {
        super.pause();
        Gdx.app.log(LOG, "Pausing game");
    }

    @Override
    public void resume() {
        super.resume();
        Gdx.app.log(LOG, "Resuming game");
    }

    @Override
    public void setScreen(Screen screen)
    {
        super.setScreen(screen);
        Gdx.app.log(LOG, "Setting screen: " + GameHelper.getSimpleName(screen.getClass()));
    }

    @Override
    public void dispose() {
        super.dispose();
        Gdx.app.log(LOG, "Disposing game");
    }
}

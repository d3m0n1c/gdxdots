package ua.vydai.gdxdots.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GameScreen extends AbstractScreen {
    Texture splashTexture;
    TextureRegion splashTextureRegion;

    public GameScreen(Game game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();
        splashTexture = new Texture("data/libgdx.png");
        splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        splashTextureRegion = new TextureRegion(splashTexture, 0, 0, 512, 275);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        batch.begin();
        batch.draw(splashTextureRegion, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();
    }

    @Override
    public void dispose() {
        splashTexture.dispose();
        super.dispose();
    }
}

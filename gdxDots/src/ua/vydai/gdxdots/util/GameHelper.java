package ua.vydai.gdxdots.util;

public class GameHelper {
    private GameHelper() {
    }

    public static String getSimpleName(Class<?> clazz) {
        String className = clazz.getName();
        return className.substring(className.lastIndexOf('.') + 1);
    }
}
